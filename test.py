import urlparse
import base64
import ldap

from suds.transport.https import WindowsHttpAuthenticated
from suds.client import Client
from suds.sax.element import Element
from suds.cache import ObjectCache

from suds.plugin import DocumentPlugin


### Tips ###
# 1.
# Error
#   The element 'FindItem' in namespace 'http://schemas.microsoft.com/exchange/services/2006/messages'
#   has invalid child element 'Restriction' in namespace 'http://schemas.microsoft.com/exchange/services/2006/messages'
# probably means, that order of parameters is invalid


class _AddService(DocumentPlugin):
    # WARNING: suds hides exceptions in plugins
    # this plugin is taken from 'ewsclient' project
    #    tip: try to 'rm -r /tmp/suds' if seems that this plugin don't work
    def loaded(self, ctx):
        """Add missing service."""
        urlprefix = urlparse.urlparse(ctx.url)
        service_url = urlparse.urlunparse(urlprefix[:2] + ('/EWS/Exchange.asmx', '', '', ''))
        servicexml = \
            '<wsdl:service name="ExchangeServices">'\
            '   <wsdl:port name="ExchangeServicePort" binding="tns:ExchangeServiceBinding">'\
            '      <soap:address location="%s"/>'\
            '    </wsdl:port>'\
            '  </wsdl:service>'\
            '</wsdl:definitions>' % service_url
        ctx.document = ctx.document.replace('</wsdl:definitions>', servicexml.encode('utf-8'))
        return ctx


class EWS(object):
    def __init__(self, username, password, server, ldapServer=None, ldapBaseDN=None):
        """
        example of arguments:
          ('test2@ad.zojax.com', 'the_password', 'https://ad.zojax.com', 'ldap://ad.zojax.com', 'dc=ad,dc=zojax,dc=com')

        NOTE: if you provide ldap parameters, username and password should be acceptable for ldap
        i.e. username should be in form 'name@domain.com'
        """
        self._original_server = server
        server = urlparse.urlunparse(('https', urlparse.urlparse(server).netloc, '/EWS/Services.wsdl', '', '', ''))
        transport = WindowsHttpAuthenticated(username=username, password=password)
        self.client = Client(url=server, username=username, password=password, transport=transport,
                             plugins=[_AddService()])
        self.client.set_options(cache=ObjectCache())
        self.client.options.cache.setduration(days=10)
        if ldapBaseDN is not None and ldapServer is not None:
            # TODO process 'ldapServer' argument as in function userLdap.loginLdap
            self.ldap_obj = ldap.initialize(ldapServer)
            self.ldap_obj.protocol_version = ldap.VERSION3
            self.ldap_obj.simple_bind_s(username, password)
            self.ldap_obj.set_option(ldap.OPT_REFERRALS, 0)
        else:
            self.ldap_obj = None
        self.ldap_base_dn = ldapBaseDN

    def _produceElement(self, source, namespaces=None):
        """
        produce Suds-applicable Element from source

        Args:
            source -- list or tuple. Possible formats:
                (name{str}, attributes{dict}, content{str})
                (name{str}, attributes{dict}, sub_source)
                (name{str}, attributes{dict}, sub_source1, sub_source2, ...)
            where sub_sourceN has the same format, as source, or sub_sourceN is Element by itself

        Return:
            object of Suds 'Element' class
        """
        name, attrs = source[0], source[1]
        content = source[2:]
        elem = Element(name)
        if namespaces is not None:
            elem.nsprefixes.update(namespaces)
        for k, v in attrs.iteritems():
            elem.set(k, v)
        if content:
            if isinstance(content[0], basestring):
                if content[0]:
                    elem.setText(content[0])
            else:
                for sub_elem in content:
                    if isinstance(sub_elem, Element):
                        elem.append(sub_elem)
                    else:
                        elem.append(self._produceElement(sub_elem))
        return elem

    @staticmethod
    def _wrap(xml):
        """
        Generate the necessary boilerplate XML for a raw SOAP request.
        The XML is specific to the server version.

        this function is taken from EWSWrapper project
        https://github.com/maiiku/EWSWrapper_py/blob/master/EWSWrapper.py
        """
        return '<?xml version="1.0" encoding="UTF-8"?>'\
               '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" '\
               'xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types" '\
               'xmlns:m="http://schemas.microsoft.com/exchange/services/2006/messages">'\
               '<s:Header/>'\
               '<s:Body>%s</s:Body>' \
               '</s:Envelope>' % xml

    def _makeListFromResponseItems(self, items, itemName):
        if hasattr(items, itemName):
            results = getattr(items, itemName)
        elif hasattr(items, 'Item'):
            results = getattr(items, 'Item')
        else:  # empty response
            results = []
        if not isinstance(results, list):
            # means that there is only one item in response
            results = [results]
        return results

    def _makeItemIds(self, ids):
        items_ids = ['m:ItemIds', {}]
        for msgid in ids:
            items_ids.append(('t:ItemId', {'Id': msgid}))
        return self._produceElement(items_ids)

    def _makeMailbox(self, address=None, name=None):
        sub_elements = []
        if address is not None:
            sub_elements.append(('t:EmailAddress', {}, address))
        if name is not None:
            sub_elements.append(('t:Name', {}, name))
        assert len(sub_elements) != 0, 'You should provide at least address or name'
        return self._produceElement(['t:Mailbox', {}] + sub_elements + [('t:MailboxType', {}, 'Mailbox')])

    def makeParentFolderIdsByDistId(self, folder='inbox', changekey=None):
        """
        make <ParentFolderIds> element for getIds function
        Args:
            folder -- unique name of folder. complete list of possible values see here:
            http://msdn.microsoft.com/en-us/library/aa580808%28v=exchg.140%29.aspx
        """
        attrs = {'Id': folder}
        if changekey is not None:
            attrs['ChangeKey'] = changekey
        return self._produceElement(['m:ParentFolderIds', {}, ['t:DistinguishedFolderId', attrs]])

    def makeParentFolderIdsByFolderId(self, _id, changekey=None):
        """
        make <ParentFolderIds> element for getIds function
        Args:
            _id, changekey -- values, returned by getFolders function (id and changekey for folder)
        """
        attrs = {'Id': _id}
        if changekey is not None:
            attrs['ChangeKey'] = changekey
        return self._produceElement(('m:ParentFolderIds', {}, ('t:FolderId', attrs)))

    def getIds(self, parent_folder_ids, datetimeFrom=None, datetimeTo=None, maxEntriesReturned=1000, offset=0):
        """
        get IDs of all messages in folder
        using SOAP operation 'FindItem'

        Args:
            parent_folder_ids -- folder to search, result of makeParentFolderIdsByDistId or
                makeParentFolderIdsByFolderId function
            datetimeFrom -- first date
            datetimeTo -- last date
            maxEntriesReturned -- max number of items to be returned
            offset -- identifies which item will be the first to be delivered in the response

        Returns:
            bool IncludesLastItemInRange, list with IDs
        """
        def makeDateRestriction(date, conditional_operator):
            return self._produceElement((conditional_operator, {},
                                         ('t:FieldURI', {'FieldURI': 'item:DateTimeCreated'}),
                                         ('t:FieldURIOrConstant', {},
                                          ('t:Constant', {'Value': date.strftime('%Y-%m-%dT%H:%M:%SZ')}))))

        shape_id_only = self._produceElement(['m:ItemShape', {}, ['t:BaseShape', {}, 'IdOnly']])

        # handle restrictions
        restrictions = []
        if datetimeFrom:
            restrictions.append(makeDateRestriction(datetimeFrom, 't:IsGreaterThanOrEqualTo'))
        if datetimeTo:
            restrictions.append(makeDateRestriction(datetimeTo, 't:IsLessThanOrEqualTo'))
        restriction = None
        if restrictions:
            if len(restrictions) == 1:
                restriction = restrictions[0]
            else:
                restriction = self._produceElement(['t:And', {}] + restrictions)
            restriction = self._produceElement(('m:Restriction', {}, restriction))

        # handle pagination
        page_view = self._produceElement(('m:IndexedPageItemView',
                                          dict(MaxEntriesReturned=maxEntriesReturned,
                                               Offset=offset, BasePoint='Beginning')))

        # handle sort order
        sort_order = self._produceElement(('m:SortOrder', {},
                                           ('t:FieldOrder', {'Order': 'Ascending'},
                                            ('t:FieldURI', {'FieldURI': 'item:DateTimeCreated'}))))

        if restriction:
            find_item = self._produceElement(('m:FindItem', {'Traversal': 'Shallow'},
                                              shape_id_only, page_view, restriction, sort_order, parent_folder_ids))
        else:
            find_item = self._produceElement(('m:FindItem', {'Traversal': 'Shallow'},
                                              shape_id_only, page_view, sort_order, parent_folder_ids))

        response = self.client.service.FindItem(__inject={'msg': self._wrap(find_item)})
        includes_last_item = response.FindItemResponseMessage.RootFolder._IncludesLastItemInRange
        results = self._makeListFromResponseItems(response.FindItemResponseMessage.RootFolder.Items, 'Message')
        return [item.ItemId._Id for item in results], includes_last_item

    def getFolders(self, onlyNonEmpty=False, mailbox=None):
        """
        get IDs, changekeys and display names of folders with class 'IPF.Note'
        using SOAP operation 'FindFolder'
        the search is performed recursively

        Args:
            onlyNonEmpty -- return only non-empty folders
            mailbox -- Mailbox Element. by default is used mailbox of current user

        Returns:
            list of tuples (folder_id, folder_changekey, folder_display_name)
        """
        restrictions = [('t:IsEqualTo', {},
                         ('t:FieldURI', {'FieldURI': 'folder:FolderClass'}),
                         ('t:FieldURIOrConstant', {}, ('t:Constant', {'Value': 'IPF.Note'})))]
        if onlyNonEmpty:
            restrictions.append(('t:IsGreaterThan', {},
                                 ('t:FieldURI', {'FieldURI': 'folder:TotalCount'}),
                                 ('t:FieldURIOrConstant', {}, ('t:Constant', {'Value': '0'}))))
        restriction = restrictions[0] if len(restrictions) == 1 else self._produceElement(['t:And', {}] + restrictions)
        restriction = self._produceElement(('m:Restriction', {}, restriction))

        find_folder = self._produceElement(('m:FindFolder', {'Traversal': 'Deep'},
                                            ('m:FolderShape', {},
                                             ('t:BaseShape', {}, 'IdOnly'),
                                             ('t:AdditionalProperties', {},
                                              ('t:FieldURI', {'FieldURI': 'folder:DisplayName'}))),
                                            restriction,
                                            ('m:ParentFolderIds', {},
                                             ('t:DistinguishedFolderId', {'Id': 'root'},
                                              '' if mailbox is None else mailbox)),))
        response = self.client.service.FindFolder(__inject={'msg': self._wrap(find_folder)})
        results = self._makeListFromResponseItems(response.FindFolderResponseMessage.RootFolder.Folders, 'Folder')
        return [(folder.FolderId._Id, folder.FolderId._ChangeKey, folder.DisplayName) for folder in results]

    def getMessages(self, msgids):
        """
        get Mime content only
        using SOAP operation 'GetItem'

        Args:
            msgids -- list of messages ids

        Return:
            dictionary <msgid>: <content of message>
            WARNING: it's possible that not all messages will be fetched
        """
        assert not isinstance(msgids, basestring)
        item_shape = self._produceElement(['m:ItemShape', {},
            ['t:BaseShape', {}, 'IdOnly'],
            ['t:IncludeMimeContent', {}, 'true'],
            ])#['t:AdditionalProperties', {}]])
        items_ids = self._makeItemIds(msgids)
        get_item = self._produceElement(('m:GetItem', {}, item_shape, items_ids))
        response = self.client.service.GetItem(__inject={'msg': self._wrap(get_item)})
        results = response.GetItemResponseMessage
        if not isinstance(results, list):  # means that there is only one message in mailbox
            results = [results]
        return {item.Items.Message.ItemId._Id: base64.decodestring(item.Items.Message.MimeContent.value)
                for item in results if item.Items}

    def deleteMessages(self, msgids, deleteType='HardDelete'):
        """
        delete messages

        Args:
            msgids -- ids of messages to delete
            deleteType -- type of deletion. possible values: 'HardDelete', 'SoftDelete', 'MoveToDeletedItems'
                for details see http://msdn.microsoft.com/en-us/library/aa562961%28v=exchg.140%29.aspx

        Returns:
            list of ids of successfully deleted messages
        """
        assert not isinstance(msgids, basestring)
        assert deleteType in ['HardDelete', 'SoftDelete', 'MoveToDeletedItems'],\
            'Wrong deleteType argument. See docstring.'
        item_ids = self._makeItemIds(msgids)
        delete_items = self._produceElement(('m:DeleteItem',
                                             dict(DeleteType=deleteType,
                                                  SendMeetingCancellations='SendToNone',
                                                  AffectedTaskOccurrences='SpecifiedOccurrenceOnly'),
                                             item_ids))
        response = self.client.service.DeleteItem(__inject={'msg': self._wrap(delete_items)})
        if not isinstance(response.DeleteItemResponseMessage, list):
            results = [response.DeleteItemResponseMessage]
        else:
            results = response.DeleteItemResponseMessage
        return [msgid for i, msgid in enumerate(msgids) if results[i]._ResponseClass == 'Success']

    def getExhcnageUsers(self):
        """
        raises ldap.LDAPError

        Returns:
            list of exchange users. each element of list is tuple (mail, login, name)
            if ldap parameters was not provided in __init__, empty list is returned
        """
        if self.ldap_obj is None:
            return []
        searchScope = ldap.SCOPE_SUBTREE
        attributes = ["mail", "name", "cn", "userPrincipalName", "sAMAccountName"]
        # filter is taken from here
        # http://www.msexchange.org/articles-tutorials/exchange-server-2000/management-administration/Creating_a_list_of_Users_and_their_email_addresses_in_Exchange_2000_2.html
        filter = '(&(mailnickname=*)(|(&(objectCategory=person)(objectClass=user)(!(homeMDB=*))' \
                 '(!(msExchHomeServerName=*)))(&(objectCategory=person)(objectClass=user)(|(homeMDB=*)' \
                 '(msExchHomeServerName=*)))))'
        users = self.ldap_obj.search_s(self.ldap_base_dn, searchScope, filter, attributes)
        results = []
        for _, user_attrs in users:
            if 'mail' in user_attrs:
                mail = user_attrs['mail'][0]

                if 'userPrincipalName' in user_attrs:
                    login = user_attrs["userPrincipalName"][0]
                elif 'sAMAccountName' in user_attrs:
                    login = user_attrs["sAMAccountName"][0]
                else:
                    login = ''

                if 'name' in user_attrs:
                    name = user_attrs["name"][0]
                elif 'cn' in user_attrs:
                    name = user_attrs["cn"][0]
                else:
                    name = ''

                results.append((mail, login, name))
        return results

    def getMailboxes(self):
        """
        Returns:
            list of mailboxes. mailbox then can be passed to getFolders method
            if ldap parameters was not provided in __init__, empty list is returned
        """
        return [self._makeMailbox(mail) for mail, login, name in self.getExhcnageUsers()]


import os
# i'm storing password in file 'pwd' in my home folder to not commit it to the repository :3
with open(os.environ['HOME'] + '/pwd') as pwd_file:
    password = pwd_file.read().strip()
username = 'test2@ad.zojax.com'
url = 'https://173.236.51.114/EWS/Services.wsdl'
ldap_url = 'ldap://ad.zojax.com'
ldap_base_dn = 'dc=ad,dc=zojax,dc=com'

ews = EWS(username, password, url, ldap_url, ldap_base_dn)
print 'OK'
#usrs = ews.getExhcnageUsers(username, password, 'ldap://ad.zojax.com', 'dc=ad,dc=zojax,dc=com')


#ids, inc_last = getIds()
#m = getMessages([ids[0]])
#print m[ids[0]]


# /home/vsevolod/Desktop/tmp_prog/disposable/ews/lib/python2.7/site-packages/ntlm/HTTPNtlmAuthHandler.py
    #if ',' in auth_header_value:
    #    auth_header_value, postfix = auth_header_value.split(',', 1)
    #(ServerChallenge, NegotiateFlags) = ntlm.parse_NTLM_CHALLENGE_MESSAGE(auth_header_value[5:])
    #user_parts = user.split('\\', 1)
    #DomainName = user_parts[0].upper() if len(user_parts) > 1 else ''
    #UserName = user_parts[-1]
